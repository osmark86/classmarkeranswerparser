from bs4 import BeautifulSoup as bs
from lxml import etree
import sys, getopt

objectList = []
list_feedback_tags = ['gray v3QuizHolder editor', 'gray bbcode v3QuizHolder editor', 'gray  v3QuizHolder editor']
list_question_tags = ['qsholder editor', 'qsholder bbcode editor']

def wrongArgs():
    print("You didn't provide the required arguments correctly! Allowed formats are as follows:")
    print("FORMAT: python answerGrabber.py <example.html>")
    print("FORMAT: python answerGrabber.py <example.html> <output.xml>")
    sys.exit()

class Result:
    def __init__(self, num, question, answer, feedback):
        self.num = num
        self.question = question
        self.answer = answer
        self.feedback = feedback

    def __repr__(self):
        return "<num:%s q:%s> a:%s fb:%s\n" % (self.num, self.question, self.answer, self.feedback)

def find_answer(answer_list, index_pos):
    for tag in ['saans', 'saans editor', 'saans bbcode editor']:
        try:
            return answer_list[index_pos].find('div', class_=tag).string
        except AttributeError:
            continue
    return None

def find_paragraph(main_list_item, some_list):
    for fbs in some_list:
        try:
            return main_list_item.find('p', class_=fbs).text
        except AttributeError:
            continue
    return None

def main(argv):
    if not argv or len(argv) > 2 or argv[0].split(".")[-1] != "html":
        wrongArgs()
    try:
        htmlFile = open(argv[0],'r').read()
    except IOError as e:
        print(e)
        sys.exit()

    soup = bs(htmlFile, 'html.parser')
    div_qd_list = soup.findAll('div', class_='qd')

    for item in div_qd_list:
        question_nr = item.h2.contents[0].split(" ")[1]
        question_final = find_paragraph(item, list_question_tags)
        chosen_list = []
        for y in item.findAll('p', class_='chosen'):
            chosen_list.extend(y.findAll(text=True))
            chosen = chosen_list[1].split(" ")
            if (chosen[-1] == ""):
                chosen.pop(-1)
        answer = item.findAll('div', class_='sarow')
        answers = ""
        for z in chosen:
            if z == "A)":
                ans = find_answer(answer,0)
            elif z == "B)":
                ans = find_answer(answer,1)
            elif z == "C)":
                ans = find_answer(answer,2)
            elif z == "D)":
                ans = find_answer(answer,3)
            elif z == "E)":
                ans = find_answer(answer,4)
            answers += str(ans) + "\n"

        feedbacks_final = find_paragraph(item, list_feedback_tags)

        final_result = Result(question_nr, question_final, answers, feedbacks_final)
        objectList.append(final_result)

    if len(argv) == 1:
        check = True
        for yy in objectList:
            print(yy.__repr__())
        while check:
            user_input = input("\nDo you wish to generate XML file of the results? (Y/N)").lower()
            if user_input == "y":
                while True:
                    file_name_input = input("Please enter a filename in the format <example.xml>")
                    try:
                        file_name_input_check = file_name_input.split('.')
                    except ValueError:
                        continue
                    if len(file_name_input_check) < 2:
                        file_name_input += ".xml"
                        generateXML(objectList, file_name_input)
                    else:
                        if file_name_input_check[-1] == "xml":
                            generateXML(objectList, file_name_input)
                        else:
                            file_name_joined = ".".join(file_name_input_check)
                            file_name_joined += ".xml"
                            generateXML(objectList, file_name_joined)
            elif user_input == "n":
                print("k bye!")
                check = False
            else:
                print("That is not a valid choice!")

    if len(argv) == 2:
        if argv[1].split(".")[-1] == "xml":
            generateXML(objectList, argv[1])
        else:
            ext_added = argv[1] + ".xml"
            generateXML(objectList, ext_added)

def generateXML(lst, argv):
    root = etree.Element("root")
    doc = etree.ElementTree(root)
    for i in lst:
        s = etree.SubElement(root, "Section")
        n = etree.SubElement(s, "QuestionNumber")
        q = etree.SubElement(s, "Question")
        a = etree.SubElement(s, "Answer")
        f = etree.SubElement(s, "Feedback")
        n.text = i.num
        q.text = i.question
        a.text = i.answer
        f.text = i.feedback
    try:
        doc.write(argv, xml_declaration=True, encoding='UTF-8')
        print(argv + " successfully generated!")
    except:
        print("The file could not be generated!")
    sys.exit()

if __name__ == "__main__":
    main(sys.argv[1:])
