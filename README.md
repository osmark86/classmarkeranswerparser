# README #

This explains how to install all necessary dependencies and how to run the script.

### What is this repository for? ###

*** Quick summary**

This repo is for creating xml-files from html answers fetched from www.classmarker.com. Specifically for SCRUM tests since I haven't designed or tested for any other tests and don't know whether or not their DOM structures are the same or not.

The following is a link to the specific test in question [SCRUM Quiz](https://www.classmarker.com/online-test/start/?quiz=vek54a6ec10658ef) accessed via https://www.scrum.org/Assessments/Open-Assessments

*** Version**

1.0.0


### How do I get set up? ###

*** Summary of set up**

The script is written for Python 3.5.X which needs to be installed in order to run. It is also terminal-based which requires you to set the $PATH for python if you are a windows user. Google instructions or try referring to this stackoverflow question -> [How to add to the pythonpath in windows 7?](http://stackoverflow.com/questions/3701646/how-to-add-to-the-pythonpath-in-windows-7)

The script does not make any HTTP calls to the server for the answers (because of server-side scripts fetching answers) so in order to properly run the script make sure that you save the answer html-page with a .html-extension locally (such as "myResults.html") **in the same directory** as the script, i e /My/Directory/classmarkerAnswerParser.py.

Now you are ready to run the script from the terminal. Navigate to the same directory as the script and run either of the following commandline arguments making sure that you reference the html-file you saved in the same directory correctly:


```
#!terminal

python classmarkerAnswerParser.py <<htmlFile.html>> <<outputfile.xml>>
```

or


```
#!terminal

python classmarkerAnswerParser.py <<htmlFile.html>>
```

The first argument <<htmlFile.html>> should be your html-file and the second [*optional*] argument is the name you wish to give to your output xml-file.

If you only supply an html-file as a single argument, the questions and answers are displayed directly in your terminal with the option to convert results to an xml-file.

The output file is generated into the same directory as the location of the script.

The generated xml-files can then be imported into your spreadsheet software of choice, such as MS Excel.

Happy parsing!

*** Dependencies**

In order to run this script you will need the following python dependencies (I haven't bothered making a setup.py file for handling this yet, but that may change in the future..). The versions used by this script is written next to each dependency as well:

* Beautiful Soup 4 <4.5.1>
* lxml <3.7.2>

These dependencies can be easily installed using Python's pip installer by typing in the following commands into your terminal:


```
#!terminal

pip install beautifulsoup4
```

and...

```
#!terminal

pip install lxml
```

With these lovely dependencies installed onto your machine the script should work fine and dandy! (with the exception for undiscovered bugs due to not having tested on any other OS other than Win 7 Enterprise Edition and any extra as of yet undiscovered html tag classes not accounted for...)


### Contribution guidelines ###

Feel free to contribute! No clucks given. ^^

### Who do I talk to? ###

* Anyone who's willing to listen
* God if you're feeling lucky